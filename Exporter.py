from configparser import ConfigParser, NoOptionError, NoSectionError #loading/saving settings
import logging
import requests
import json
import time
import datetime
from math import ceil
from os.path import isdir as isDir
from os.path import isfile as isFile
from os import sep as dirSep
from os import makedirs as makedir
from os import listdir
from os.path import getctime
from shutil import rmtree as removeDir

__author__ = 'vka'

class Exporter():
    def __init__(self):

        self.api_url = 'http://ws.audioscrobbler.com/2.0/'
        self.userData = None

        self.setLogger()
        self.log.info('-' * 56)
        self.log.info("App init")

        self.config = {}
        self.loadConfig()
        self.log.debug('Current config:')
        for att, val in self.config.items():
            self.log.debug('{}  \t{}'.format(att, val))

        if self.config['rewrite'] and isDir(self.config['saveDir']):
            self.log.info('Existing backup found. Removing.')
            removeDir(self.config['saveDir'])

        self.log.debug('Time to call lastfm and get some user data.')
        self.getUserData()

        if not isDir(self.config['saveDir']):
            makedir(self.config['saveDir'])

        with open(self.config['saveDir'] + dirSep + self.config['username'] + '.txt', 'w', encoding='utf8') as file:
            file.write(json.dumps(self.userData, indent = 4))
            file.close()
            self.log.info('Saved user info to file {}{}{}.txt'.format(
                self.config['saveDir'], dirSep, self.config['username']
            ))

        self.log.debug('Time to get tracks info now.')
        self.loadTracks()

    def loadConfig(self):
        try:
            config = ConfigParser()
            config.read('config.cfg')

            self.config['username'] = config.get('config', 'username')
            self.config['apikey'] = config.get('config', 'api_key')
            self.config['saveDir'] = config.get('config', 'save_dir')
            self.config['rewrite'] = config.getboolean('config', 'rewrite')
            self.config['logginglevel'] = config.get('config', 'logging_level')

            if self.config['logginglevel'] == 'debug':
                self.log.getLogger().setLevel(logging.DEBUG)
                logging.getLogger('requests').setLevel(logging.DEBUG)
            elif self.config['logginglevel'] == 'warning':
                self.log.getLogger().setLevel(logging.WARNING)
                logging.getLogger('requests').setLevel(logging.WARNING)
            elif self.config['logginglevel'] == 'error':
                self.log.getLogger().setLevel(logging.ERROR)
                logging.getLogger('requests').setLevel(logging.ERROR)
            else:
                self.log.getLogger().setLevel(logging.INFO)
                logging.getLogger('requests').setLevel(logging.WARNING)

            self.log.info('Config loaded')

        except (NoSectionError, NoOptionError):
            self.log.warning('Config file invalid or not existing. Loading default data.')
            self.saveDefaultConfig()
            self.loadConfig()

    def saveDefaultConfig(self):

        config = ConfigParser()

        config.add_section('config')
        config.set('config', 'api_key', '54da813483696ce443d4a87ed1cbdfd5')
        config.set('config', 'username', 'voodka_')
        config.set('config', 'save_dir', 'voodka_')
        config.set('config', 'rewrite', 'yes')
        config.set('config', 'logging_level', 'info')

        with open('config.cfg', 'w', encoding='utf8') as file:
            config.write(file)
        self.log.info('Saved default config')

    def setLogger(self):
        self.log = logging
        self.log.basicConfig(format='%(asctime)s\t%(levelname)s\t%(message)s', datefmt='%H:%M:%S',
                             level=logging.DEBUG)

    def getUserData(self):
        url = self.api_url + '?method=user.getInfo&user={}&api_key={}&format=json'\
            .format(self.config['username'], self.config['apikey'])
        self.log.debug('Connecting with: ' + url)

        self.userData = requests.get(url).json()

        self.log.info('Got user data')

    def loadTracks(self):
        #first, we need to figure out what data we need
        regdate = self.userData['user']['registered']['#text']
        regdate = datetime.datetime.strptime(regdate, '%Y-%m-%d %H:%M')
        regtime = time.mktime(regdate.timetuple())
        curtime = time.mktime(datetime.datetime.now().timetuple())
        self.log.debug('Loading data from {} to {}'.format(regtime, curtime))

        first = datetime.datetime.strptime("{} {}".format(regdate.year, regdate.month),
                                           "%Y %m")
        mktime = time.mktime(first.timetuple())
        neededMonths = []

        while mktime < curtime:
            cur = datetime.datetime.fromtimestamp(mktime)
            year, month = cur.year, cur.month
            yearstr = str(year)
            monthstr = str(month)
            if len(monthstr) == 1:
                monthstr = '0' + monthstr
            filename = "{}{}{}-{}.txt".format(self.config['saveDir'], dirSep,
                                              yearstr, monthstr)
            if (self.config['rewrite']) or (not isFile(filename))\
                or (year == datetime.datetime.now().year and month == datetime.datetime.now().month)\
                or (isFile(filename) and int(getctime(filename)) < int(cur.timestamp())):
                neededMonths.append(yearstr + '-' + monthstr)

            month += 1
            if month == 13:
                year += 1
                month = 1
            next = datetime.datetime.strptime("{} {}".format(year, month),
                                              "%Y %m")
            mktime = time.mktime(next.timetuple())
        if neededMonths == []:
            self.log.debug('All up to date')
        else:
            self.log.debug('Needed months: ' + ', '.join(neededMonths))

        for m in neededMonths:
            self.loadMonth(m)

    def loadMonth(self, m):
        start = datetime.datetime.strptime(m, "%Y-%m")
        self.log.info('Loading month ' + start.strftime("%Y-%m"))

        starttime = start.timestamp()
        endmonth = start.month + 1
        endyear = start.year
        if endmonth == 13:
            endmonth = 1
            endyear += 1
        endmonth = str(endmonth)
        if len(endmonth) == 1:
            endmonth = '0' + endmonth
        endyear = str(endyear)

        endtime = datetime.datetime.strptime(endyear + ' ' + endmonth, '%Y %m')
        endtime = endtime.timestamp()
        #init check for # pages
        url = self.api_url + '?method=user.getRecentTracks&limit=1&format=json&user={}&api_key={}' \
                             '&from={}&to={}'\
            .format(self.config['username'], self.config['apikey'], starttime, endtime)
        r = requests.get(url).json()

        pagesData = []
        pages = ceil(int(r['recenttracks']['@attr']['total']) / 200.0)
        for p in range(1, pages + 1):
            self.log.info('Loading page {} of {} ({}%)'
                .format(p, pages, round(100.0 * (p - 1) / pages)))
            pagesData.append(self.getPage(starttime, endtime, p))

        num = 0
        monthData = {'@attr': {}, 'tracks': []}
        for page in pagesData:
            num += 1
            if num == 1:
                monthData['@attr'] = {'user':   page['recenttracks']['@attr']['user'],
                                     'total':   page['recenttracks']['@attr']['total'],
                                     'curtime': int(datetime.datetime.now().timestamp())
                                      }

            for track in page['recenttracks']['track']:
                try:
                    if track['@attr']['nowplaying'] == 'true':
                        continue
                except:
                    pass
                newTrack = {}
                for key in track.keys():
                    if key != 'image' and key != 'streamable':
                        newTrack[key] = track[key]
                monthData['tracks'].append(newTrack)

        filename = self.config['saveDir'] + dirSep + m + '.txt'
        with open(filename, 'w', encoding='utf8') as file:
            #file.write(json.dumps(monthData, indent=4))
            json.dump(monthData, file, ensure_ascii=False)
            file.close()
        self.log.info('Month data saved successfully')

    def getPage(self, s, e, p):
        url = self.api_url + '?method=user.getRecentTracks&limit=200&format=json&user={}&api_key={}' \
                             '&from={}&to={}&page={}'\
            .format(self.config['username'], self.config['apikey'], s, e, p)
        return requests.get(url).json()
def main():
    e = Exporter()

if __name__ == '__main__':
    main()